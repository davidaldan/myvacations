﻿# Host: localhost  (Version 5.5.5-10.1.26-MariaDB)
# Date: 2018-08-13 11:12:18
# Generator: MySQL-Front 6.0  (Build 2.20)


#
# Structure for table "places"
#

CREATE TABLE `places` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `likes` int(11) DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "places"
#

INSERT INTO `places` VALUES (1,'São Paulo',5,'http://www.apidev.muchomil.com/hotelmanager/public/images/test/card-saopaolo.png',1,NULL,NULL),(2,'Amsterdam',4,'http://www.apidev.muchomil.com/hotelmanager/public/images/test/card-amsterdam.png',1,NULL,NULL),(3,'San Francisco',6,'http://www.apidev.muchomil.com/hotelmanager/public/images/test/card-sf.png',1,NULL,NULL),(4,'Madison',10,'http://www.apidev.muchomil.com/hotelmanager/public/images/test/card-madison.png',1,NULL,NULL),(5,'Mexico',50,'http://www.apidev.muchomil.com/hotelmanager/public/images/test/card-sf.png',2,NULL,NULL);

#
# Structure for table "places_status"
#

CREATE TABLE `places_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "places_status"
#

INSERT INTO `places_status` VALUES (1,'Active'),(2,'Inactive');

#
# Structure for table "visit_places"
#

CREATE TABLE `visit_places` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `place_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "visit_places"
#

INSERT INTO `visit_places` VALUES (1,1,'Parque do Ibirapuera','https://travel.usnews.com/dims4/USNEWS/0c0c2f0/2147483647/resize/637x426%3E/quality/85/?url=/static-travel/images/destinations/304/parque_do_ibirapuera.jpg',1,NULL,NULL),(2,1,'Pinacoteca do Estado','https://travel.usnews.com/dims4/USNEWS/3fa7268/2147483647/resize/637x426%3E/quality/85/?url=/static-travel/images/destinations/304/pinacoteca_do_estado.jpg',1,NULL,NULL),(3,1,'Theatro Municipal de São Paulo','https://travel.usnews.com/dims4/USNEWS/ac6e7a4/2147483647/resize/637x426%3E/quality/85/?url=/static-travel/images/destinations/304/theatreo_municipal.jpg',1,NULL,NULL),(4,2,'Amsterdam Evening Canal Cruise with 4-Course Dinner and Drinks','https://cache-graphicslib.viator.com/graphicslib/thumbs674x446/2544/SITours/amsterdam-evening-canal-cruise-with-4-course-dinner-and-drinks-in-amsterdam-118218.jpg',1,NULL,NULL),(5,3,'Golden Gate Bridge','https://travel.usnews.com/static-travel/images/destinations/20/gettyimages-123318669.jpg',1,NULL,NULL),(6,4,'Olbrich Botanical Gardens','https://media-cdn.tripadvisor.com/media/photo-s/00/1b/b6/e2/thai-pavilion-and-gardens.jpg',1,NULL,NULL),(7,5,'Xcaret','https://www.xcaret.com/img/thumbs/en/actividades-incluidas/underground-rivers-thumb.jpg',1,NULL,NULL);
